'use strict'

const config = {
  // verbose: true,
  verbose: false,
  testMatch: [
    '**/__tests__/**/*.[jt]s?(x)',
    // '**/?(*.)+(spec|test).[jt]s?(x)',
    // '**/?(*.)+(spec|test).[cm][jt]s?(x)',
    '**/?(*.)+(spec|test).?(c|m)[jt]s?(x)',
  ],
  clearMocks: true,
  restoreMocks: true,
}

module.exports = config
