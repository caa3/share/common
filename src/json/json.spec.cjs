'use strict'

const { readJson, writeJson } = require('./index.cjs')
const { resolve } = require('path')
const { deleteFile, isExistsFilePath } = require('../path/index.cjs')

describe('json.cjs', () => {
  // constants
  const existJsonFiles = [resolve('.', '__test__', 'data', 'test.json')]
  const nonExistJsonFiles = [resolve('.', '__test__', 'data', 'dummy.json')]
  const tmpJsonFiles = [resolve('.', '__test__', 'data', 'tmp.json')]
  const cannotCreateJsonFiles = [
    resolve('.', '__test__', 'data', 'dummy', 'tmp.json'),
  ]
  const existNonJsonFiles = [resolve('.', '__test__', 'data', 'test.txt')]
  const validEncodingStrings = ['utf8', new String('utf8')]
  const invalidEncodingStrings = ['EUC-JP', new String('EUC-JP')]

  // readJson() testings.
  describe('readJson', () => {
    existJsonFiles.forEach((val) => {
      validEncodingStrings.forEach((encoding) => {
        test(`readJson(existJsonFile:${val},validEncodingString:${encoding}) to be normal end`, async () => {
          await readJson(val, { encoding })
        })
      })
    })
    existJsonFiles.forEach((val) => {
      invalidEncodingStrings.forEach((encoding) => {
        test(`readJson(existJsonFile:${val},invalidEncodingString:${encoding}) to throw error`, async () => {
          await expect(() => readJson(val, { encoding })).rejects.toThrow()
        })
      })
    })
    nonExistJsonFiles.forEach((val) => {
      test(`readJson(nonExistJsonFile:${val}) to throw error`, async () => {
        await expect(() => readJson(val)).rejects.toThrow()
      })
    })
    existNonJsonFiles.forEach((val) => {
      test(`readJson(existNonJsonFile:${val}) to throw error`, async () => {
        await expect(() => readJson(val)).rejects.toThrow()
      })
    })
  })

  // writeJson() testings.
  describe('writeJson', () => {
    const writeObjects = [{ prop: 'test' }]

    beforeEach(async () => {
      tmpJsonFiles.forEach(async (val) => {
        if (isExistsFilePath(val)) await deleteFile(val)
      })
    })

    afterEach(async () => {
      tmpJsonFiles.forEach(async (val) => {
        if (isExistsFilePath(val)) await deleteFile(val)
      })
    })

    tmpJsonFiles.forEach((val) => {
      writeObjects.forEach((object) => {
        test(`writeJson(tmpJsonFile:${val},writeObject:${object}) to be normal end`, async () => {
          await writeJson(val, object)
        })
      })
    })
    cannotCreateJsonFiles.forEach((val) => {
      writeObjects.forEach((object) => {
        test(`writeJson(cannotCreateJsonFile:${val},writeObject:${object}) to throw error`, async () => {
          await expect(() => writeJson(val, object)).rejects.toThrow()
        })
      })
    })
  })
})
