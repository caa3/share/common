'use strict'

const fs = require('fs')
const iconv = require('iconv-lite')
const { isExistsFilePath, isExistsDirectoryPath } = require('../path/index.cjs')
const { isValidEncodingType, ENCODING_TYPES } = require('../type/index.cjs')

// error messages.
const ERROR_MSGS = {
  PATH_IS_INVALID: 'file path might be empty, invalid type or not exists.',
  ENCODING_TYPE_IS_INVALID: 'encoding type is invalid.',
  COLUMNS_SHOULD_BOOL: 'columns is should be boolean.',
}

// write json function.
const writeJson = async (path, object) => {
  if (!isExistsDirectoryPath(path)) throw Error(ERROR_MSGS.PATH_IS_INVALID)
  const dataString = JSON.stringify(object, null, '\t')
  await fs.promises.writeFile(path, dataString)
}

// read json function.
const readJson = async (
  path,
  { encoding = ENCODING_TYPES.DEFAULT_TYPE } = {}
) => {
  // validations.
  if (!isExistsFilePath(path)) throw Error(ERROR_MSGS.PATH_IS_INVALID)
  if (!isValidEncodingType(encoding))
    throw Error(ERROR_MSGS.ENCODING_TYPE_IS_INVALID)

  // read file.
  const buffer = await fs.promises.readFile(path)
  const decodedString = iconv.decode(buffer, encoding)
  return Promise.resolve(JSON.parse(decodedString))
}

module.exports = { writeJson, readJson }
