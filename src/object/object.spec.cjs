'use strict'

const { cloneObjDeeply } = require('./index.cjs')

describe('object.cjs', () => {
  // constants
  const objects = [
    { prop: 0 },
    { prop: [] },
    { propA: 0, propB: 1 },
    { prop: { childProp: 0 } },
    { prop: { childProp: [] } },
    { prop: { childPropA: {}, childPropB: [] } },
  ]

  const objectA = { propA: 'A' }
  const objectB = { propB: 'B' }
  objectA['propB'] = objectB
  objectB['propA'] = objectA
  const recurciveObjects = [objectA, objectB]

  // cloneObjDeeply() testings.
  describe('cloneObjDeeply', () => {
    objects.forEach((val) => {
      test(`cloneObjDeeply(object:${val}) is to equal copied object`, () => {
        expect(cloneObjDeeply(val)).toEqual(val)
      })
    })
    recurciveObjects.forEach((val) => {
      test(`cloneObjDeeply(recurciveObject:${val}) is to equal copied object`, () => {
        expect(cloneObjDeeply(val)).toEqual(val)
      })
    })
  })
})
