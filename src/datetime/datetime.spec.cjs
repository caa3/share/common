'use strict'

const {
  TIME_ZONE_TYPES,
  isTimeZoneOffsetRegex,
  getTimeZoneOffsetMillSec,
  getISOString,
  getNow,
  delayTime,
} = require('./index.cjs')

describe('datetime.cjs', () => {
  // constants
  const timeZoneOffsetRegexStrings = [
    '+09:00',
    '-09:00',
    '09:00',
    '+24:00',
    '-24:00',
    '24:00',
    '+23:59',
    '-23:59',
    '23:59',
    '+00:00',
    '-00:00',
    '00:00',
    new String('+09:00'),
  ]
  const nonTimeZoneOffsetRegexStrings = [
    'Z',
    '+09:00Z',
    'Z+09:00',
    '+09:0',
    '-09:0',
    '09:0',
    '+9:00',
    '-9:00',
    '9:00',
    '+0900',
    '-0900',
    '0900',
    '+09:',
    '-09:',
    '09:',
    '+:00',
    '-:00',
    ':00',
    new String('Z'),
    new String('+09:00Z'),
  ]
  const numbers = [0, new Number(0)]
  const timeZoneOffsetRegexObjects = [
    {
      timeZoneOffsetRegexString: '+09:00',
      timeZoneOffsetMillSec: 9 * 60 * 60 * 1000,
    },
    {
      timeZoneOffsetRegexString: '-09:00',
      timeZoneOffsetMillSec: -9 * 60 * 60 * 1000,
    },
    {
      timeZoneOffsetRegexString: '09:00',
      timeZoneOffsetMillSec: 9 * 60 * 60 * 1000,
    },
    {
      timeZoneOffsetRegexString: '+15:15',
      timeZoneOffsetMillSec: 15 * 60 * 60 * 1000 + 15 * 60 * 1000,
    },
    {
      timeZoneOffsetRegexString: '-15:15',
      timeZoneOffsetMillSec: -(15 * 60 * 60 * 1000 + 15 * 60 * 1000),
    },
    {
      timeZoneOffsetRegexString: '15:15',
      timeZoneOffsetMillSec: 15 * 60 * 60 * 1000 + 15 * 60 * 1000,
    },
  ]

  // isTimeZoneOffsetRegex() testings.
  describe('isTimeZoneOffsetRegex()', () => {
    timeZoneOffsetRegexStrings.forEach((val) => {
      test(`isTimeZoneOffsetRegex(timeZoneOffsetRegexString:${val}) is to be true`, () => {
        expect(isTimeZoneOffsetRegex(val)).toBe(true)
      })
    })
    nonTimeZoneOffsetRegexStrings.forEach((val) => {
      test(`isTimeZoneOffsetRegex(nonTimeZoneOffsetRegexString:${val}) is to be false`, () => {
        expect(isTimeZoneOffsetRegex(val)).toBe(false)
      })
    })
    numbers.forEach((val) => {
      test(`isTimeZoneOffsetRegex(number:${val}) is to be false`, () => {
        expect(isTimeZoneOffsetRegex(val)).toBe(false)
      })
    })
  })

  // getTimeZoneOffsetMillSec() testings.
  describe('getTimeZoneOffsetMillSec()', () => {
    timeZoneOffsetRegexObjects.forEach((val) => {
      test(`getTimeZoneOffsetMillSec(timeZoneOffsetRegexString:${val.timeZoneOffsetRegexString}) is to be calculated integer number:${val.timeZoneOffsetMillSec})`, () => {
        expect(getTimeZoneOffsetMillSec(val.timeZoneOffsetRegexString)).toBe(
          val.timeZoneOffsetMillSec
        )
      })
    })
    nonTimeZoneOffsetRegexStrings.forEach((val) => {
      test(`getTimeZoneOffsetMillSec(nonTimeZoneOffsetRegexString:${val}) is to be 0`, () => {
        expect(getTimeZoneOffsetMillSec(val)).toBe(0)
      })
    })
  })

  // getNow() testings.
  describe('getNow', () => {
    // 2022-08-30T04:24:54.716Z = 1661833494716
    const mockTimeMillSec = new Date('2022-08-30T04:24:54.716Z').getTime()
    beforeEach(() => {
      jest.spyOn(Date, 'now')
      Date.now.mockReturnValue(mockTimeMillSec)
    })
    test(`getNow() is to be mock time ${mockTimeMillSec}`, () => {
      expect(getNow()).toBe(1661833494716)
    })
  })

  // getISOString() testings.
  describe('getISOString', () => {
    // 2022-08-30T04:24:54.716Z = 1661833494716
    const utcISOString = '2022-08-30T04:24:54.716Z'
    const jstISOString = '2022-08-30T13:24:54.716+09:00'
    const mockTimeMillSec = new Date(utcISOString).getTime()
    const defaultOptions = [undefined, {}]
    const defaultStrings = [TIME_ZONE_TYPES.DEFAULT]
    const utcStrings = ['UTC', 'utc', TIME_ZONE_TYPES.UTC]
    const jstStrings = ['JST', 'jst', TIME_ZONE_TYPES.JST]
    const nonTimeZoneStrings = ['', 'aaa']
    beforeEach(() => {
      jest.spyOn(Date, 'now')
      Date.now.mockReturnValue(mockTimeMillSec)
    })
    defaultOptions.forEach((val) => {
      test(`getISOString(${val}) is to be ${utcISOString}`, () => {
        expect(getISOString(val)).toBe(utcISOString)
      })
    })
    defaultStrings.forEach((val) => {
      test(`getISOString({ timeZone: '${val}'}) is to be ${utcISOString}`, () => {
        expect(getISOString({ timeZone: val })).toBe(utcISOString)
      })
    })
    utcStrings.forEach((val) => {
      test(`getISOString({ timeZone: '${val}'}) is to be ${utcISOString}`, () => {
        expect(getISOString({ timeZone: val })).toBe(utcISOString)
      })
    })
    jstStrings.forEach((val) => {
      test(`getISOString({ timeZone: '${val}'}) is to be ${jstISOString}`, () => {
        expect(getISOString({ timeZone: val })).toBe(jstISOString)
      })
    })
    nonTimeZoneStrings.forEach((val) => {
      test(`getISOString({ timeZone: '${val}'}) is to throw error`, () => {
        expect(() => getISOString({ timeZone: val })).toThrow()
      })
    })
  })

  // delayTime() testings.
  describe('delayTime', () => {
    // 2022-08-30T04:24:54.716Z = 1661833494716
    const delayMillSec = 500
    const nonMillSec = 'dummy'
    beforeEach(() => {
      jest.useFakeTimers()
      jest.spyOn(global, 'setTimeout')
    })
    test(`delayTime(${delayMillSec}) is to wait ${delayMillSec} msec`, async () => {
      expect(setTimeout).toHaveBeenCalledTimes(0)
      await delayTime(delayMillSec)
      expect(setTimeout).toHaveBeenCalledTimes(1)
      expect(setTimeout).toHaveBeenLastCalledWith(undefined, delayMillSec)
    })
    test(`delayTime(${nonMillSec}) is to throw error`, async () => {
      await expect(() => delayTime(nonMillSec)).rejects.toThrow()
    })
  })
})
