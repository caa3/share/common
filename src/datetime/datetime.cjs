'use strict'

// error massages.
const ERROR_MSGS = {
  INVALID_TIME_ZONE_TYPE: 'input time zone type is invalid. input: ${0}.',
  INVALID_TIME: 'input time is invalid. input: ${0}.',
}

// delay time.
const delayTime = async (time) => {
  if (!Number.isInteger(time))
    throw new Error(`${ERROR_MSGS.INVALID_TIME}`.replace('${0}', time))
  return new Promise((resolve) => setTimeout(resolve(), parseInt(time)))
}

// time zone types. values are time zone keys.
const TIME_ZONE_TYPES = {
  JST: 'JST',
  UTC: 'UTC',
  DEFAULT: 'UTC',
}

// time zone offset values.
const TIME_ZONE_OFFSET = {
  JST: '+09:00',
  UTC: 'Z',
  DEFAULT: 'Z',
}

// check time zone offset regex. Z is false.
const isTimeZoneOffsetRegex = (value) => {
  const timeZoneOffsetRegex =
    /^(?<sign>[+-])?(?<hh>[0-2][0-9]):(?<mm>[0-5][0-9])$/
  return (
    (typeof value === 'string' ||
      (typeof value === 'object' && value instanceof String)) &&
    timeZoneOffsetRegex.test(value)
  )
}

// get time zone offset millisecond.
const getTimeZoneOffsetMillSec = (value) => {
  const OFFSET_NONE = 0
  const OFFSET_HOUR = 60 * 60 * 1000
  const OFFSET_MINUTE = 60 * 1000
  const timeZoneOffsetRegex =
    /^(?<sign>[+-])?(?<hh>[0-2][0-9]):(?<mm>[0-5][0-9])$/
  if (!isTimeZoneOffsetRegex(value)) return OFFSET_NONE
  const sign = value.match(timeZoneOffsetRegex).groups.sign === '-' ? -1 : 1
  return (
    sign *
    (parseInt(value.match(timeZoneOffsetRegex).groups.hh) * OFFSET_HOUR +
      parseInt(value.match(timeZoneOffsetRegex).groups.mm) * OFFSET_MINUTE)
  )
}

// Date.now()
const getNow = () => {
  return Date.now()
}

// TODO add args value of iso8601String and millisecondsFromEpoch
// get iso8601 string.
const getISOString = ({ timeZone = TIME_ZONE_TYPES.DEFAULT } = {}) => {
  if (
    !Object.values(TIME_ZONE_TYPES).some((value) => {
      return value === timeZone.toUpperCase()
    })
  )
    throw new Error(
      `${ERROR_MSGS.INVALID_TIME_ZONE_TYPE}`.replace('${0}', timeZone)
    )

  const nowDateOffset = new Date(
    getNow() +
      getTimeZoneOffsetMillSec(TIME_ZONE_OFFSET[timeZone.toUpperCase()])
  )
  const nowISOString =
    nowDateOffset.toISOString().split('Z')[0] +
    TIME_ZONE_OFFSET[timeZone.toUpperCase()]
  return nowISOString
}

// TODO add method for utc to locale iso8601String
// convert UTC To Locale iso 8601 string

module.exports = {
  delayTime,
  TIME_ZONE_TYPES,
  isTimeZoneOffsetRegex,
  getTimeZoneOffsetMillSec,
  getISOString,
  getNow,
}
