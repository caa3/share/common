'use strict'

const { ENCODING_TYPES, isValidEncodingType } = require('./index.cjs')

describe('type.cjs', () => {
  // constants
  const validEncodingStrings = [
    'utf8',
    'Shift_JIS',
    new String('utf8'),
    new String('Shift_JIS'),
    ...Object.values(ENCODING_TYPES),
  ]
  const invalidEncodingStrings = ['EUC-JP', new String('EUC-JP')]

  // isValidEncodingType() testings.
  describe('isValidEncodingType', () => {
    validEncodingStrings.forEach((val) => {
      test(`isValidEncodingType(validEncodingString:${val}) is to be true`, () => {
        expect(isValidEncodingType(val)).toBe(true)
      })
    })
    invalidEncodingStrings.forEach((val) => {
      test(`isValidEncodingType(invalidEncodingString:${val}) is to be false`, () => {
        expect(isValidEncodingType(val)).toBe(false)
      })
    })
  })
})
