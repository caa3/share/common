'use strict'

// encoding types.
const ENCODING_TYPES = {
  DEFAULT_TYPE: 'utf8',
  UTF8: 'utf8',
  SHIFT_JIS: 'Shift_JIS',
}

// validate encoding type.
const isValidEncodingType = (encoding) => {
  return (
    (typeof encoding === 'string' ||
      (typeof encoding === 'object' && encoding instanceof String)) &&
    Object.keys(ENCODING_TYPES).some((key) => {
      return (
        ENCODING_TYPES[key] === encoding ||
        ENCODING_TYPES[key] === encoding.valueOf()
      )
    })
  )
}

module.exports = {
  isValidEncodingType,
  ENCODING_TYPES,
}
