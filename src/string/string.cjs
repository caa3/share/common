'use strict'

// validate is true string function.
const isTrueString = (value) => {
  return typeof value === 'string' && value.toLowerCase() === 'true'
}

// validate is true string function.
const isFalseString = (value) => {
  return typeof value === 'string' && value.toLowerCase() === 'false'
}

// check integer string.
const isIntegerString = (value) => {
  const intRegex = /^[+-]?[0-9]?$/
  return (
    ((typeof value === 'number' || value instanceof Number) &&
      intRegex.test(value.toString())) ||
    ((typeof value === 'string' || value instanceof String) &&
      intRegex.test(value))
  )
}

// check float string.
const isFloatString = (value) => {
  const floatRegex =
    /^[+-]?((0|[1-9][0-9]*)(\.[0-9]*)?|\.[0-9]+)([eE][+-]?[0-9]+)?$/
  return (
    ((typeof value === 'number' || value instanceof Number) &&
      floatRegex.test(value.toString())) ||
    ((typeof value === 'string' || value instanceof String) &&
      floatRegex.test(value))
  )
}

module.exports = {
  isTrueString,
  isFalseString,
  isIntegerString,
  isFloatString,
}
