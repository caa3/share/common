'use strict'

const {
  isTrueString,
  isFalseString,
  isIntegerString,
  isFloatString,
} = require('./index.cjs')

describe('string.cjs', () => {
  // constants
  const trueStrings = ['true', 'True', 'TRUE']
  const falseStrings = ['false', 'False', 'FALSE']
  const integerStrings = [0, '0', new Number(0), new String('0')]
  const floatStrings = [0.1, '0.1', new Number(0.1), new String('0.1')]

  // isTrueString() testings.
  describe('isTrueString', () => {
    trueStrings.forEach((val) => {
      test(`isTrueString(trueString:${val}) is to be true`, () => {
        expect(isTrueString(val)).toBe(true)
      })
    })
    falseStrings.forEach((val) => {
      test(`isTrueString(falseString:${val}) is to be false`, () => {
        expect(isTrueString(val)).toBe(false)
      })
    })
    integerStrings.forEach((val) => {
      test(`isTrueString(integerString:${val}) is to be false`, () => {
        expect(isTrueString(val)).toBe(false)
      })
    })
    floatStrings.forEach((val) => {
      test(`isTrueString(floatString:${val}) is to be false`, () => {
        expect(isTrueString(val)).toBe(false)
      })
    })
  })

  // isFalseString() testings.
  describe('isFalseString', () => {
    trueStrings.forEach((val) => {
      test(`isFalseString(trueString:${val}) is to be false`, () => {
        expect(isFalseString(val)).toBe(false)
      })
    })
    falseStrings.forEach((val) => {
      test(`isFalseString(falseString:${val}) is to be true`, () => {
        expect(isFalseString(val)).toBe(true)
      })
    })
    integerStrings.forEach((val) => {
      test(`isFalseString(integerString:${val}) is to be false`, () => {
        expect(isFalseString(val)).toBe(false)
      })
    })
    floatStrings.forEach((val) => {
      test(`isFalseString(floatString:${val}) is to be false`, () => {
        expect(isFalseString(val)).toBe(false)
      })
    })
  })

  // isIntegerString() testings.
  describe('isIntegerString', () => {
    trueStrings.forEach((val) => {
      test(`isIntegerString(trueString:${val}) is to be false`, () => {
        expect(isIntegerString(val)).toBe(false)
      })
    })
    falseStrings.forEach((val) => {
      test(`isIntegerString(falseString:${val}) is to be false`, () => {
        expect(isIntegerString(val)).toBe(false)
      })
    })
    integerStrings.forEach((val) => {
      test(`isIntegerString(integerString:${val}) is to be true`, () => {
        expect(isIntegerString(val)).toBe(true)
      })
    })
    floatStrings.forEach((val) => {
      test(`isIntegerString(floatString:${val}) is to be false`, () => {
        expect(isIntegerString(val)).toBe(false)
      })
    })
  })

  // isFloatString() testings.
  describe('isFloatString', () => {
    trueStrings.forEach((val) => {
      test(`isFloatString(trueString:${val}) is to be false`, () => {
        expect(isFloatString(val)).toBe(false)
      })
    })
    falseStrings.forEach((val) => {
      test(`isFloatString(falseString:${val}) is to be false`, () => {
        expect(isFloatString(val)).toBe(false)
      })
    })
    integerStrings.forEach((val) => {
      test(`isFloatString(integerString:${val}) is to be true`, () => {
        expect(isFloatString(val)).toBe(true)
      })
    })
    floatStrings.forEach((val) => {
      test(`isFloatString(floatString:${val}) is to be true`, () => {
        expect(isFloatString(val)).toBe(true)
      })
    })
  })
})
