'use strict'

const {
  isZero,
  isEmptyString,
  isNull,
  isUndefined,
  isEmptyArray,
  isEmptyObject,
  isEmpty,
  isNotEmpty,
} = require('./index.cjs')

describe('empty.cjs', () => {
  // constants
  const zeroNumbers = [0, new Number(0)]
  const nonZeroNumbers = [1, new Number(1)]
  const emptyStrings = ['', new String('')]
  const nonEmptyStrings = [' ', new String(' ')]
  const emptyObjects = [{}, new Object()]
  const nonEmptyObjects = [{ prop: 0 }, new Object({ prop: 0 })]
  const emptyArrays = [[], new Array(0), new Array(1), Array(0), Array(1)]
  const nonEmptyArrays = [[0], new Array(0, 1), Array(0, 1)]
  const nulls = [null]
  const undefineds = [undefined]
  const nans = [NaN]

  // isZero() testings.
  describe('isZero', () => {
    zeroNumbers.forEach((val) => {
      test(`isZero(zeroNumber:${val}) is to be true`, () => {
        expect(isZero(val)).toBe(true)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isZero(nonZeroNumber:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isZero(emptyString:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isZero(nonEmptyString:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isZero(emptyObject:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isZero(nonEmptyObject:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isZero(emptyArray:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isZero(nonEmptyArray:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isZero(null:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    undefineds.forEach((val) => {
      test(`isZero(undefined:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
    nans.forEach((val) => {
      test(`isZero(nan:${val}) is to be false`, () => {
        expect(isZero(val)).toBe(false)
      })
    })
  })

  // isEmptyString() testings.
  describe('isEmptyString', () => {
    zeroNumbers.forEach((val) => {
      test(`isEmptyString(zeroNumber:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isEmptyString(nonZeroNumber:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isEmptyString(emptyString:${val}) is to be true`, () => {
        expect(isEmptyString(val)).toBe(true)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isEmptyString(nonEmptyString:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isEmptyString(emptyObject:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isEmptyString(nonEmptyObject:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isEmptyString(emptyArray:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isEmptyString(nonEmptyArray:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isEmptyString(null:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    undefineds.forEach((val) => {
      test(`isEmptyString(undefined:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
    nans.forEach((val) => {
      test(`isEmptyString(nan:${val}) is to be false`, () => {
        expect(isEmptyString(val)).toBe(false)
      })
    })
  })

  // isNull() testings.
  describe('isNull', () => {
    zeroNumbers.forEach((val) => {
      test(`isNull(zeroNumber:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isNull(nonZeroNumber:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isNull(emptyString:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isNull(nonEmptyString:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isNull(emptyObject:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isNull(nonEmptyObject:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isNull(emptyArray:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isNull(nonEmptyArray:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isNull(null:${val}) is to be true`, () => {
        expect(isNull(val)).toBe(true)
      })
    })
    undefineds.forEach((val) => {
      test(`isNull(undefined:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
    nans.forEach((val) => {
      test(`isNull(nan:${val}) is to be false`, () => {
        expect(isNull(val)).toBe(false)
      })
    })
  })

  // isUndefined() testings.
  describe('isUndefined', () => {
    zeroNumbers.forEach((val) => {
      test(`isUndefined(zeroNumber:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isUndefined(nonZeroNumber:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isUndefined(emptyString:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isUndefined(nonEmptyString:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isUndefined(emptyObject:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isUndefined(nonEmptyObject:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isUndefined(emptyArray:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isUndefined(nonEmptyArray:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isUndefined(null:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
    undefineds.forEach((val) => {
      test(`isUndefined(undefined:${val}) is to be true`, () => {
        expect(isUndefined(val)).toBe(true)
      })
    })
    nans.forEach((val) => {
      test(`isUndefined(nan:${val}) is to be false`, () => {
        expect(isUndefined(val)).toBe(false)
      })
    })
  })

  // isEmptyArray() testings.
  describe('isEmptyArray', () => {
    zeroNumbers.forEach((val) => {
      test(`isEmptyArray(zeroNumber:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isEmptyArray(nonZeroNumber:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isEmptyArray(emptyString:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isEmptyArray(nonEmptyString:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isEmptyArray(emptyObject:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isEmptyArray(nonEmptyObject:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isEmptyArray(emptyArray:${val}) is to be true`, () => {
        expect(isEmptyArray(val)).toBe(true)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isEmptyArray(nonEmptyArray:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isEmptyArray(null:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    undefineds.forEach((val) => {
      test(`isEmptyArray(undefined:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
    nans.forEach((val) => {
      test(`isEmptyArray(nan:${val}) is to be false`, () => {
        expect(isEmptyArray(val)).toBe(false)
      })
    })
  })

  // isEmptyObject() testings.
  describe('isEmptyObject', () => {
    zeroNumbers.forEach((val) => {
      test(`isEmptyObject(zeroNumber:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isEmptyObject(nonZeroNumber:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isEmptyObject(emptyString:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isEmptyObject(nonEmptyString:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isEmptyObject(emptyObject:${val}) is to be true`, () => {
        expect(isEmptyObject(val)).toBe(true)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isEmptyObject(nonEmptyObject:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isEmptyObject(emptyArray:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isEmptyObject(nonEmptyArray:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isEmptyObject(null:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    undefineds.forEach((val) => {
      test(`isEmptyObject(undefined:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
    nans.forEach((val) => {
      test(`isEmptyObject(nan:${val}) is to be false`, () => {
        expect(isEmptyObject(val)).toBe(false)
      })
    })
  })

  // isEmpty() testings.
  describe('isEmpty', () => {
    zeroNumbers.forEach((val) => {
      test(`isEmpty(zeroNumber:${val}) is to be true`, () => {
        expect(isEmpty(val)).toBe(true)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isEmpty(nonZeroNumber:${val}) is to be false`, () => {
        expect(isEmpty(val)).toBe(false)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isEmpty(emptyString:${val}) is to be true`, () => {
        expect(isEmpty(val)).toBe(true)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isEmpty(nonEmptyString:${val}) is to be false`, () => {
        expect(isEmpty(val)).toBe(false)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isEmpty(emptyObject:${val}) is to be true`, () => {
        expect(isEmpty(val)).toBe(true)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isEmpty(nonEmptyObject:${val}) is to be false`, () => {
        expect(isEmpty(val)).toBe(false)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isEmpty(emptyArray:${val}) is to be true`, () => {
        expect(isEmpty(val)).toBe(true)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isEmpty(nonEmptyArray:${val}) is to be false`, () => {
        expect(isEmpty(val)).toBe(false)
      })
    })
    nulls.forEach((val) => {
      test(`isEmpty(null:${val}) is to be true`, () => {
        expect(isEmpty(val)).toBe(true)
      })
    })
    undefineds.forEach((val) => {
      test(`isEmpty(undefined:${val}) is to be true`, () => {
        expect(isEmpty(val)).toBe(true)
      })
    })
    nans.forEach((val) => {
      test(`isEmpty(nan:${val}) is to be false`, () => {
        expect(isEmpty(val)).toBe(false)
      })
    })
  })

  // isNotEmpty() testings.
  describe('isNotEmpty', () => {
    zeroNumbers.forEach((val) => {
      test(`isNotEmpty(zeroNumber:${val}) is to be false`, () => {
        expect(isNotEmpty(val)).toBe(false)
      })
    })
    nonZeroNumbers.forEach((val) => {
      test(`isNotEmpty(nonZeroNumber:${val}) is to be true`, () => {
        expect(isNotEmpty(val)).toBe(true)
      })
    })
    emptyStrings.forEach((val) => {
      test(`isNotEmpty(emptyString:${val}) is to be false`, () => {
        expect(isNotEmpty(val)).toBe(false)
      })
    })
    nonEmptyStrings.forEach((val) => {
      test(`isNotEmpty(nonEmptyString:${val}) is to be true`, () => {
        expect(isNotEmpty(val)).toBe(true)
      })
    })
    emptyObjects.forEach((val) => {
      test(`isNotEmpty(emptyObject:${val}) is to be false`, () => {
        expect(isNotEmpty(val)).toBe(false)
      })
    })
    nonEmptyObjects.forEach((val) => {
      test(`isNotEmpty(nonEmptyObject:${val}) is to be true`, () => {
        expect(isNotEmpty(val)).toBe(true)
      })
    })
    emptyArrays.forEach((val) => {
      test(`isNotEmpty(emptyArray:${val}) is to be false`, () => {
        expect(isNotEmpty(val)).toBe(false)
      })
    })
    nonEmptyArrays.forEach((val) => {
      test(`isNotEmpty(nonEmptyArray:${val}) is to be true`, () => {
        expect(isNotEmpty(val)).toBe(true)
      })
    })
    nulls.forEach((val) => {
      test(`isNotEmpty(null:${val}) is to be false`, () => {
        expect(isNotEmpty(val)).toBe(false)
      })
    })
    undefineds.forEach((val) => {
      test(`isNotEmpty(undefined:${val}) is to be false`, () => {
        expect(isNotEmpty(val)).toBe(false)
      })
    })
    nans.forEach((val) => {
      test(`isNotEmpty(nan:${val}) is to be true`, () => {
        expect(isNotEmpty(val)).toBe(true)
      })
    })
  })
})
