'use strict'

// validate is empty function.
const isEmpty = (value) => {
  return (
    isZero(value) ||
    isEmptyString(value) ||
    isNull(value) ||
    isUndefined(value) ||
    isEmptyArray(value) ||
    isEmptyObject(value)
  )
}

// validate is not empty function.
const isNotEmpty = (value) => {
  return !isEmpty(value)
}

// validate is number zero function.
const isZero = (value) => {
  return (
    (typeof value === 'number' && value === 0) ||
    (value instanceof Number && value.valueOf() === 0)
  )
}

// validate is empty string function.
const isEmptyString = (value) => {
  return (
    (typeof value === 'string' && value === '') ||
    (value instanceof String && value.valueOf() === '')
  )
}

// validate is null function.
const isNull = (value) => {
  return typeof value === 'object' && value === null
}

// validate is undefined function.
const isUndefined = (value) => {
  return typeof value === 'undefined'
}

// validate is empty array function.
const isEmptyArray = (value) => {
  return (
    typeof value === 'object' &&
    !(value instanceof Number) &&
    !(value instanceof String) &&
    value !== null &&
    Array.isArray(value) &&
    Object.keys(value).length === 0
  )
}
// validate is empty object function.
const isEmptyObject = (value) => {
  return (
    typeof value === 'object' &&
    !(value instanceof Number) &&
    !(value instanceof String) &&
    value !== null &&
    !Array.isArray(value) &&
    Object.keys(value).length === 0
  )
}

module.exports = {
  isEmpty,
  isNotEmpty,
  isZero,
  isEmptyString,
  isEmptyObject,
  isEmptyArray,
  isNull,
  isUndefined,
}
