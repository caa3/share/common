'use strict'

const fs = require('fs')
const iconv = require('iconv-lite')
const { parse } = require('csv-parse')
const { isExistsFilePath } = require('../path/index.cjs')
const { isValidEncodingType, ENCODING_TYPES } = require('../type/index.cjs')

// error messages.
const ERROR_MSGS = {
  PATH_IS_INVALID: 'file path might be empty, invalid type or not exists.',
  ENCODING_TYPE_IS_INVALID: 'encoding type is invalid.',
  COLUMNS_SHOULD_BOOL: 'columns is should be boolean.',
}

// read csv function.
const readCsv = async (
  path,
  { encoding = ENCODING_TYPES.DEFAULT_TYPE, columns = false } = {}
) => {
  // validations.
  if (!isExistsFilePath(path)) throw Error(ERROR_MSGS.PATH_IS_INVALID)
  if (!isValidEncodingType(encoding))
    throw Error(ERROR_MSGS.ENCODING_TYPE_IS_INVALID)
  if (typeof columns !== 'boolean') throw Error(ERROR_MSGS.COLUMNS_SHOULD_BOOL)

  // read file.
  const buffer = await fs.promises.readFile(path)
  const decodedString = iconv.decode(buffer, encoding)
  return new Promise((resolve, reject) => {
    parse(decodedString, { columns }, (err, rows) => {
      if (err) {
        reject(err)
      }
      resolve(rows)
    })
  })
}

module.exports = {
  readCsv,
}
