'use strict'

const { readCsv } = require('./index.cjs')
const { resolve } = require('path')

describe('csv.cjs', () => {
  // constants
  const existCsvFiles = [resolve('.', '__test__', 'data', 'test.csv')]
  const nonExistCsvFiles = [resolve('.', '__test__', 'data', 'dummy.csv')]
  const existNonCsvFiles = [resolve('.', '__test__', 'data', 'test.json')]
  const validEncodingStrings = ['utf8', new String('utf8')]
  const invalidEncodingStrings = ['EUC-JP', new String('EUC-JP')]
  const invalidColumns = ['true', 'false']

  // readCsv() testings.
  describe('readCsv', () => {
    existCsvFiles.forEach((val) => {
      validEncodingStrings.forEach((encoding) => {
        test(`readCsv(existCsvFile:${val},validEncodingString:${encoding}) to be normal end`, async () => {
          await readCsv(val, { encoding })
        })
      })
    })
    existCsvFiles.forEach((val) => {
      invalidEncodingStrings.forEach((encoding) => {
        test(`readCsv(existCsvFile:${val},invalidEncodingString:${encoding}) to throw error`, async () => {
          await expect(() => readCsv(val, { encoding })).rejects.toThrow()
        })
      })
    })
    existCsvFiles.forEach((val) => {
      invalidColumns.forEach((columns) => {
        test(`readCsv(existCsvFile:${val},invalidColumns:${columns}) to throw error`, async () => {
          await expect(() => readCsv(val, { columns })).rejects.toThrow()
        })
      })
    })

    nonExistCsvFiles.forEach((val) => {
      test(`readCsv(nonExistCsvFile:${val}) to throw error`, async () => {
        await expect(() => readCsv(val)).rejects.toThrow()
      })
    })
    existNonCsvFiles.forEach((val) => {
      test(`readCsv(existNonCsvFile:${val}) to throw error`, async () => {
        await expect(() => readCsv(val)).rejects.toThrow()
      })
    })
  })
})
