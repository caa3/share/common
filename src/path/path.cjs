'use strict'

const fs = require('fs')
const { dirname } = require('path')
const { isNotEmpty } = require('../empty/index.cjs')

// error massages.
const ERROR_MSGS = {
  INVALID_FILE_PATH: 'input path is invalid. input: ${0}.',
}

// validate exists file path.
const isExistsFilePath = (path) => {
  return (
    (isNotEmpty(path) && typeof path === 'string' && fs.existsSync(path)) ||
    (typeof path === 'object' &&
      path instanceof String &&
      fs.existsSync(path.valueOf()))
  )
}

// validate exists directory path.
const isExistsDirectoryPath = (path) => {
  return (
    (isNotEmpty(path) &&
      typeof path === 'string' &&
      fs.existsSync(dirname(path))) ||
    (typeof path === 'object' &&
      path instanceof String &&
      fs.existsSync(dirname(path.valueOf())))
  )
}

// delete file path.
const deleteFile = async (path) => {
  if (!isExistsFilePath(path))
    throw new Error(`${ERROR_MSGS.INVALID_FILE_PATH}`.replace('${0}', path))
  return await fs.promises.unlink(path)
}

// touch file path.
const touchFile = async (path) => {
  const now = new Date()
  try {
    await fs.promises.utimes(path, now, now)
  } catch (error) {
    if ('ENOENT' !== error?.code) throw error
    const fh = await fs.promises.open(path, 'w')
    return await fh.close()
  }
}

module.exports = {
  isExistsFilePath,
  isExistsDirectoryPath,
  deleteFile,
  touchFile,
}
