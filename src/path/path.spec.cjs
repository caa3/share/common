'use strict'

const fs = require('fs')
const {
  deleteFile,
  isExistsDirectoryPath,
  isExistsFilePath,
  touchFile,
} = require('./index.cjs')
const { resolve } = require('path')

describe('path.cjs (path)', () => {
  // constants
  const validFilePathStrings = [
    __filename,
    './src/path/path.spec.cjs',
    new String(__filename),
    new String('./src/path/path.spec.cjs'),
  ]
  const validDirectoryPathStrings = [
    __dirname,
    './',
    new String(__dirname),
    new String('./'),
  ]
  const invalidFilePathStrings = [
    resolve(__dirname, 'dummy.cjs'),
    './src/path/path/dummy.cjs',
    new String(resolve(__dirname, 'dummy.cjs')),
    new String('./src/path/dummy.cjs'),
  ]
  const invalidDirectoryPathStrings = [
    resolve(__dirname, 'dummy/dummy.cjs'),
    './src/path/dummy/dummy.cjs',
    new String(resolve(__dirname, 'dummy/dummy.cjs')),
    new String('./src/path/dummy/dummy.cjs'),
  ]

  // isExistsFilePath() testings.
  describe('isExistsFilePath', () => {
    validFilePathStrings.forEach((val) => {
      test(`isExistsFilePath(validFilePathString:${val}) is to be true`, () => {
        expect(isExistsFilePath(val)).toBe(true)
      })
    })
    invalidFilePathStrings.forEach((val) => {
      test(`isExistsFilePath(invalidFilePathString:${val}) is to be false`, () => {
        expect(isExistsFilePath(val)).toBe(false)
      })
    })
  })

  // isExistsDirectoryPath() testings.
  describe('isExistsDirectoryPath', () => {
    validDirectoryPathStrings.forEach((val) => {
      test(`isExistsDirectoryPath(validDirectoryPathString:${val}) is to be true`, () => {
        expect(isExistsDirectoryPath(val)).toBe(true)
      })
    })
    invalidDirectoryPathStrings.forEach((val) => {
      test(`isExistsDirectoryPath(invalidDirectoryPathString:${val}) is to be false`, () => {
        expect(isExistsDirectoryPath(val)).toBe(false)
      })
    })
  })
})

describe('path.cjs (file)', () => {
  // touchFile() testings.
  const existFiles = [resolve('.', '__test__', 'data', 'test.txt')]
  const nonExistFiles = [resolve('.', '__test__', 'data', 'dummy.txt')]
  const tmpFiles = [resolve('.', '__test__', 'data', 'tmp.txt')]

  // touchFile() testings.
  describe('touchFile, normal end', () => {
    existFiles.forEach((val) => {
      test(`touchFile(existFile:${val}) to exist file, update file meta data`, async () => {
        await touchFile(val)
      })
    })
  })
  describe('touchFile, throw error (not ENOENT)', () => {
    const errorCode = 'NOT ENOENT'
    beforeEach(() => {
      jest.spyOn(fs.promises, 'utimes')
      const error = new Error(errorCode)
      error.code = errorCode
      fs.promises.utimes.mockReturnValue(Promise.reject(error))
    })
    nonExistFiles.forEach((val) => {
      test(`touchFile(existFile:${val}) to non exist file, but to throw error of not ${errorCode}`, async () => {
        await expect(() => touchFile(val)).rejects.toThrow(errorCode)
      })
    })
  })
  describe('touchFile, throw error (ENOENT)', () => {
    const errorCode = 'ENOENT'
    beforeEach(() => {
      jest.spyOn(fs.promises, 'utimes')
      const error = new Error(errorCode)
      error.code = errorCode
      fs.promises.utimes.mockReturnValue(Promise.reject(error))
    })
    nonExistFiles.forEach((val) => {
      test(`touchFile(existFile:${val}) to non exist file, but to throw error of ${errorCode}`, async () => {
        await touchFile(val)
        await deleteFile(val)
      })
    })
  })

  // deleteFile() testings.
  describe('deleteFile, throw error, ', () => {
    nonExistFiles.forEach((val) => {
      test(`deleteFile(nonExistFile:${val}) to non exist file, to throw error`, async () => {
        await expect(() => deleteFile(val)).rejects.toThrow()
      })
    })
  })
  describe('deleteFile, normal end', () => {
    tmpFiles.forEach((val) => {
      test(`touchFile(tmpFile:${val})and deleteFile(tmpFile:${val}) to tmp file, create file and delete file`, async () => {
        await touchFile(val)
        await deleteFile(val)
      })
    })
  })
})
